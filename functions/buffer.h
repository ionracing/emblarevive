/*
* Circular buffer for reading CAN messages.
*/

#ifndef BUFFER_H
#define BUFFER_H

/* Dependencies ---------------------------------------------------------- */
#include "stdint.h"
#include "eError.h"

typedef uint16_t tElement;

typedef struct 
{
    tElement *buf;  /* Block of memory*/
    uint16_t size;  /* must be a power of two */
    uint16_t read;  /* Holds current read position: 0 to (size-1)  */
    uint16_t write; /* Hods current write position: 0 to (size-1) */
}circBuffer_t;

/* Sets the variables of the bufferStruct */
void buffer_init(circBuffer_t* cb, tElement* buffer, uint16_t length);
/*wrapped subtraction to determine the length of available data */
uint16_t buffer_LengthData(circBuffer_t *cb);
enum eError buffer_write(circBuffer_t *cb, tElement data);
enum eError buffer_read(circBuffer_t *cb, tElement *data);
enum eError buffer_free(circBuffer_t *cb);

#endif
