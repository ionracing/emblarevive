#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx.h"
#include "ioMapping.h"

/* Prototypes */
void BSP_LED_init(void);

/* 
Macros: 
For turning ONN/OFF LEDs.
Example: for turning on LED1 write:
LED_ON(LED1);
*/
#define LED_ON(LED) (GPIOD->BSRRL |= LED)
#define LED_OFF(LED) (GPIOD->BSRRH |= LED)
#define LED_TOGGLE(LED) (GPIOD->ODR ^= LED)

#define LED_ALL_ON (LED_ON(LED1 | LED2 | LED3 | LED4))
#define LED_ALL_OFF (LED_OFF = LED1 | LED2 | LED3 | LED4)
#define LED_ALL_TOGGLE (LED_TOGGLE(LED1 | LED2 | LED3 | LED4))
