/* Includes */
#include "stdbool.h"
#include "stm32f4xx.h"
#include "BSP_LED.h"
#include "BSP_systick.h"
#include "stm32f4xx_tim.h"
#include "BSP_CAN.h"
#include "BSP_ADC_SIMPLE.h"
#include "core_cm4.h"
#include "ioMapping.h"
#include "BSP_ADC_SIMPLE.h"
#include "BSP_GPIO.h"

/*Function prototypes*/
static void heartbeat(void);
static void initSystem(void);

/* Constants */
#define ID_BAMOCAR 0x210
#define DLC_BAMOCAR 3
#define BAMOCAR_REPEAT_100MS 0x64

/* Data to request from bamocar. 
Currently logging:
3xTemperatures (Air, IGBT, Motor) 
3xVoltages (Vd, Vq, Vout)
1xRPM
3xCurrents (phase: a,b,c)

See manual for details:
http://www.unitek-online.de/pdf/download/Antriebe-Drive/Servo-Digital/E-DS-NDrive.pdf
*/

const uint8_t bamoCarRegLength = 10;
const uint8_t bamocarRegIDs[bamoCarRegLength] = {0x30, 0xf6, 0x54, 0x55, 0x56, 0x49, 0x4a, 0x4b, 0x2a, 0x29};


/* Main code */
int main(void)
 {
     /* Init the variables for bamocar request */
     uint8_t k = 0;
     uint8_t bamocarData[8];
     
     /* Init all the system functions (CAN, GPIO, ADC) */
     initSystem();
     
     /* Set up the variables for transmitting to the bamocar */   
     bamocarData[1] = 0x3d; /* Request ID*/
     bamocarData[3] = 0x3d; /* Repetition rate */
     
     for(k=0;k<=bamoCarRegLength; k++)
     {
         
         BSP_CAN_Tx(ID_BAMOCAR,DLC_BAMOCAR, bamocarData);
     }
     
     /* Request successfull*/
     LED_TOGGLE(LED2);
     while(1)
     {
         /* Read sensors */
         
         /* Send sensor data on CAN */
         /* Request data from Motorcontroller */
     }
 } 

/* Misc functions *****************************************************/
static void heartbeat(void)
{
    if (clk100ms == CLK_COMPLETE)
    {
        LED_TOGGLE(LED1);
        clk100ms = CLK_RESET;
	}
}

static void initSystem(void)
{
	BSP_systick_init();
	BSP_LED_init();
	BSP_ADC_SIMPLE_init();
	BSP_CAN_init();
    BSP_GPIOinit();
}
