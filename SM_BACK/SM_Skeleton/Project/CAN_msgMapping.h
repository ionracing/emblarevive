/*
 ID CAN bus list. 
 See the ID-CAN_BUS document on the drive / the documentation map
 for more information.
*/

#include "config.h"
#ifndef CAN_MSGMAPPING_H
#define CAN_MSGMAPPING_H
/* 
Common Recieve IDs
*/
/* Errors (61->600) ---------------------------------------------------*/
/* Recieve */

/* Startupt (601->800)------------------------------------------------*/
/* recieve*/
#define ID_RX_CAL_SUSPENSION_MIN 601
#define ID_RX_CAL_THROTTLE_MAX 602
#define ID_RX_CAL_THROTTLE_MIN 603
#define ID_RX_CAL_BRAKE_MIN 604
#define ID_RX_CAL_BRAKE_MAX 605
/* Sensors messages (801->1100)---------------------------------------*/
/* Recieve */
#define ID_RX_REQUEST_SENSOR_DATA 801
/* Ping (1101->1150) -------------------------------------------------*/
#define ID_RX_PING
/* Messages (1151->2047)----------------------------------------------*/
/* Recieve */

/* 
Unique transmit IDs 
*/
/* Pedalbox defines ----------------------------------------- */
#ifdef SM_PEDALS
/* Errors */
/* Recieve */
/* Transmit */
#define ID_FATAL_ERROR 91
#define ID_WARNING 92
#define D0_TC_HIGH 0
#define D0_TA_HIGH 1

/* Sensors messages*/
/* Transmit */
#define ID_THROTTLE 806
#define ID_WHEEL_SPEED_FRONT 807
#define ID_BRAKE_PRESSURE_FRONT 808
#define ID_BRAKE_PRESSURE_BACK 809
#define ID_SUSPENSION_FRONT_L 810
#define ID_SUSPENSION_FRONT_R 811
#define ID_TEMPERATURE_FRONT 812
/* Startupt */
/* Transmit */
#define ID_TX_CAL_DONE 621

/* Messages*/
#define ID_BRAKELIGHT_STATE 1202 // Used for togling the brakelight. 
                                 // d0 = 1 for brakelight on.
                                 // d0 = 0 for brakelight off.
                                 
/* battery defines ---------------------------------------------------*/
#elif defined(SM_battery)
/* Errors*/
/* Recieve */
/* Transmit */


/* Sensors messages*/
/* Recieve */

/* Startupt*/
/* recieve*/
/* Transmit */

/* Sensors messages*/ 
/* Transmit */  

/* Messages */
/* Recieve */
/* Transmit */

/* Blackbox defines -----------------------------------------------------*/
#elif defined(SM_BLACKBOX)
/* Errors */
/* Recieve */
/* Transmit */
#define ID_FATAL_ERROR  171
#define ID_WARNING      172
#define D0_TC_HIGH      0
#define D0_TA_HIGH      1
/* Sensors messages */
/* Recieve */
/* Transmit */
#define ID_T_AIR1 891 /* Mono back compartment temperature */
#define ID_T_AIR2 892 /* Mono mid (driver) temperature */
#define ID_SSRB   893 /* Suspension Right Back */
#define ID_SSLB   894 /* Suspension Left Back */
#define ID_TC1    895 /* Coolant temp; MC->Resorvoir*/
#define ID_TC2    896 /* Coolant temp; MC -> Radiator*/
#define ID_TC3    897 /* Coolant temp; Radiator -> Resorvoir */
/* Startupt */
/* recieve*/
/* Transmit */

/* Sensors messages */
/* Recieve */
/* Transmit */


/* Messages */
/* Recieve */
/* Transmit */
#endif
#endif /*END CAN_MSGMAPPING_H*/
