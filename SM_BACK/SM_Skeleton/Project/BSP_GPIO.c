#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "ioMapping.h"

void BSP_GPIOinit(void)
{
    GPIO_InitTypeDef GPIO_initStruct;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
     
    GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_initStruct.GPIO_Pin = CAN_ENABLE_PIN; // CAN ENABLE !!
    GPIO_Init(GPIOC, &GPIO_initStruct);
    
    GPIO_WriteBit(GPIOC, GPIO_Pin_10, Bit_SET); // CAN_ENABLE PIN!
     
    GPIO_initStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_initStruct.GPIO_OType = GPIO_OType_OD;
    GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_initStruct.GPIO_Pin = WHEEL_RPM_PINS;  // wheelsensors!;
    GPIO_Init(GPIOD, &GPIO_initStruct);
}

