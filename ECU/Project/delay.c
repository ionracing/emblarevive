#include "delay.h"
/* Private variables --------------------------------------------*/
volatile uint32_t ticks = 0;

/* Public functions ---------------------------------------------*/
void systickInit (uint16_t frequency)
{
	SysTick_Config(SystemCoreClock / 1000);
    NVIC_SetPriority(SysTick_IRQn, 0);
}

uint32_t get_millis(void)
{
    return ticks;
}

void delay_ms (uint32_t t)
{
    uint32_t startTest;
    uint32_t endTest;
    startTest = get_millis();
    endTest = startTest + t;
    while(startTest < endTest)
    {
        startTest = get_millis();
    }
}

/*Interrupt handler ----------------------------------------------*/
void SysTick_Handler(void)
{
    ticks++;
}


