/* Error: contains error codes for all the modules of the program 
* and functions for handling them.
*/
#ifndef EEROR_H
#define EEROR_H
enum eError
{
    eErrorNone,
    eErrorBufferFull,
    eErrorBufferEmpty,
};

#endif
