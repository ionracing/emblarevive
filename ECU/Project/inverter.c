/*
 Module for communication with the inverter.  
* Example frame for requesting data permanently:
* ID    : ID_VSI_TX
* RTR   : 0
* DLC   : 3
* D0    : REQ_REQUEST_DATA (0x3d)
* D1    : REG_DATA
* D2    : RepetitionRate in ms (i.e 0x64 for 100ms)
*/

#include "inverter.h"

/* Constants --------------------------------------------------------*/
#define N_DATA 10
/* Private types -----------------------------------------------------*/
typedef struct 
{
    uint8_t ID; /* Data ID */
    uint8_t Ts; /* Time intervall in ms */
}VSI_s;
 
/* Private variables -----------------------------------------------*/
uint8_t REG_ID[N_DATA] = 
    {
        REG_VQ, 
        REG_VD,
        REG_IQ_ACT, 
        REG_ID_ACT, 
        REG_POWER, 
        REG_SPEED, 
        REG_SPEED_FILT, 
        REG_T_MOTOR, 
        REG_T_IGBT, 
        REG_T_AIR
    };

/*Public functions --------------------------------------------------*/

/**
* @brief : Configures the inverter to start transmitting data packages. 
*/
void inverter_init(uint8_t updateRate[])
{ 
    /* Transmit request */
    for(uint8_t k = 0; k<N_DATA; k++)
    {
        CanTxMsg msg;
        msg.RTR = 0;
        msg.StdId = ID_VSI_TX;
        msg.DLC = 3;
        msg.IDE = 0;
        msg.Data[0] = REG_REQUEST_DATA;
        msg.Data[1] = REG_ID[k];
        msg.Data[2] = updateRate[k];
        if(msg.Data[2] != 0)
        {
            while(CAN_Transmit(CAN2, &msg) == CAN_TxStatus_NoMailBox)
            {
            delay_ms(100);
            }
        }
    }
}
