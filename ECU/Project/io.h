/*
* Used for initializing all the digital IO on the board and 
* functions for toggling the output states of them 
* (i.e turning on/off fans,modules,brakelight,inverter).
*/

#ifndef IO_H
#define IO_H

/* dependencies ------------------------------------------------*/
#include "BSP_gpio.h"
#include "ioMapping.h"


/* GPIO functions. 
* used with @gpioState_t to turn on/off function.
*/
typedef enum
{
    exhaustFan, /* enables/disables the fan exhaust fan*/ 
    radiatorFan, /*enables/disables the radiator fan*/ 
    can1, /* Enables/disables CAN-bus communication with the car */
    can2, /* Enables/disables CAN-bus communication with the inverter */
    inverterRunSignal, /* Enables/disables the inverter */
    brakelight, /* Turns the brakelight on/off */
    pilotline, /* Enables/disables the pilotline */
    RTDS, /* Enables/disables the ready to drive sound */
    LED1, /* Turns on/off LED1 on the PCB */
    LED2, /* Turns on/off LED2 on the PCB */
    LED3, /* Turns on/off LED3 on the PCB */
    LED4 /* Turns on/off LED4 on the PCB */
}gpioFunc_t;

/**
  * @brief Initializes all the io's on the board.  
  * @param  None 
  * @retval None
  */
void io_init(void); 

/**
  * @brief  Used for turning on/off components.
  * @param  state: The new state of the GPIO
  *          This parameter can be a value from gpioState_t
  * @param  io: the IO to change the state of. 
  * @retval None
  */
void io_Toggle( gpioFunc_t io, gpioState_t state);

#endif
