#ifndef STORAGE_H
#define STORAGE_H

/* Dependencies ----------------------------------------------------------------- */
#include "stdint.h"

/* Strcut containing all the variables to be stored*/
typedef struct 
{
    uint16_t Plim;
    uint16_t Kp;
    uint16_t Ki;
    uint16_t Kd;
    uint8_t loggUpdateRate[10];
}storageValues_t;

void storage_saveValues(storageValues_t values);
/*
* Input(s)
*   storageValues_t values - struct containing all the parameters for saving/reading.
* If stored data is corrupt: returns the storageValues_t values unchanged.
*/
storageValues_t storage_readValues(storageValues_t values);
#endif
