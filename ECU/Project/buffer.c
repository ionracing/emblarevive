#include "buffer.h"

void buffer_init(circBuffer_t* cb, tElement* buffer, uint16_t length)
{
    cb->buf = buffer;
    cb->size = length;
    cb->write = 0;
    cb->read = 0;
}

uint16_t buffer_LengthData(circBuffer_t *cb)
{
    return ((cb->write - cb->read) & (cb->size -1));
}

enum eError buffer_write(circBuffer_t *cb, tElement data)
{
    if(buffer_LengthData(cb) == (cb->size-1)){return eErrorBufferFull;}
    cb->buf[cb->write] = data;
    cb->write = (cb->write + 1) & (cb->size -1); /* Must be atomic */
    return eErrorNone;
}


enum eError buffer_read(circBuffer_t *cb, tElement *data)
{
    if (buffer_LengthData(cb) == 0){return eErrorBufferEmpty;}
    *data = cb->buf[cb->read];
    cb->read = (cb->read + 1) & (cb->size -1);
    return eErrorNone;
}
