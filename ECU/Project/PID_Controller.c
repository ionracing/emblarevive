#include "PID_Controller.h"

float saturation_add(PID * pid, float a, float b)
{
	float returnvalue = a + b;
	
	if(a < 0 && b < 0 )
	{
		if(returnvalue > 0 || returnvalue < -(pid->integrator_max))
		{
			returnvalue = -(pid->integrator_max);
		}
	} 
	else if(a > 0 && b>0 && returnvalue < 0) 
	{
		returnvalue = pid->integrator_max;
	}
	
	return returnvalue;
}


float saturation_subtract(PID * pid, float a, float b)
{
	float returnvalue = a - b;
	
	if(a < 0 && b > 0 )
	{
		if(returnvalue > 0 || returnvalue < -(pid->integrator_max))
		{
			returnvalue = -(pid->integrator_max);
		}
	} 
	else if(a > 0 && b < 0 && returnvalue < 0) 
	{
		returnvalue = pid->integrator_max;
	}
	
	return returnvalue;
}

float saturation_multiplication(PID * pid, float a, float b){
	long long returnvalue = (long long)a * (long long)b;

		if(returnvalue > pid->integrator_max)
		{
			returnvalue = pid->integrator_max;
		}
		if(returnvalue < -(pid->integrator_max))
		{
			returnvalue = -(pid->integrator_max);
		}
	return returnvalue;
}


float updatePID(PID * pid, float current_error)
{
	float pTerm, dTerm, iTerm;
	float tempValue, returnValue;
	float differentiation;
	

	pid->integral_error = saturation_add(pid, (pid->integral_error), current_error); //integral_error + current_error
	
	if(pid->integral_error < (pid->integrator_min)) //if integral_error < integral_minimum or -integral_max
	{
		pid->integral_error = (pid->integrator_min); //saturate integrator
	} 
	else if(pid->integral_error > pid->integrator_max) //same deal
	{
		pid->integral_error = pid->integrator_max;
	}
  
	differentiation = (saturation_subtract(pid, current_error, pid->prev_error)); //current_error - prev_error
	
	
	pTerm = (saturation_multiplication(pid, pid->pGain, current_error)); // pGain * current_error

	iTerm = (saturation_multiplication(pid, pid->iGain, pid->integral_error)); //iGain * integral_error
	
	dTerm = (saturation_multiplication(pid, pid->dGain, differentiation)); //dGain * differentiation
	
	
	pid->prev_error = current_error; 
	
	tempValue = saturation_add(pid, pTerm, iTerm);
	returnValue = saturation_add(pid, tempValue, dTerm);  //avoiding saturation
	
	return returnValue; //regulator output value
}
