#ifndef BSP_CAN_H
#define BSP_CAN_H

/* Dependencies -------------------------------------------- */
#include "ioMapping.h"
#include "BSP_GPIO.h"
#include "stm32f4xx_can.h"
#include "eError.h"

/* MACROS ---------------------------------------------------*/
 // byte1 = lsb, byte2 = msb
#define MASK_LSB_BYTE(data) (uint8_t)(data & 0xFF)
#define MASK_MSB_BYTE(data) (uint8_t)(data >> 8)
#define byte2halfword(byte1, byte2) ( (byte1 << 8) | (byte2 & 0xff ))
#define IS_MSG_PENDING_FIFO0(CANx) (CANx->RF0R & CAN_RF0R_FMP0)
#define IS_MSG_PENDING_FIFO1(CANx) (CANx->RF1R & CAN_RF1R_FMP1)
/* Variables ------------------------------------------------*/

/*Constants -------------------------------------------------*/
/* CAN-bus recieve ID's for the ECU */
#define cellMinVoltID 0x43
#define cellMaxVoltID 0x45
#define battVoltID    0x46
#define cellMinTempID 0x47
#define cellMaxTempID 0x49
#define battSocID     0x50
#define BmsErrorID    0x62
#define THROTTLE_ID   0x00
#define MC_MSG  0x190
#define BMS_MSG 0x7E8
/* CAN-bus Transmit ID's for the ECU */

/*Public functions ------------------------------------------*/
void BSP_CAN2_init(void);
void BSP_CAN1_init(void);
uint8_t BSP_CAN2_Tx(CanRxMsg msg);
uint8_t BSP_CAN1_Tx(CanRxMsg msg);

#endif
