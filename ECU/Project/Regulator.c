/*
* Contains the timer-IRQ for the regulator and functions
* used for the power limiting.
*/
#include "Regulator.h"
#include "BSP_GPIO.h"
/* private prototypes -----------------------------------------*/
inline static float PID(float error, float iq_max);
inline static void requestThrottle(int16_t request);
/* Private variables ------------------------------------------*/
/* Input parameters for the regulator */
float voltage;        /* DC-link voltage in car: 12bit -  */
float RPM;            /* Motor RPM */
float current;        /* Battery side current */
uint16_t throttle;       /* Throttle request from driver */

/* Regulator constants */
float p_limit; /* Maximum power of the car 0->80 kW */
/* PID constants */
float Kp; /* PID Gain */
float Kd; /* Derivative gain */
float Ki; /* Integral gain */

/*Private constants -------------------------------------------*/
#define MAX_VALUE_16BIT 65535
#define IQ_MAX 1 /* Maximum value of regulator output */

/* Macros -----------------------------------------------------*/
#define FORWARD_COUPLING(RPM, P) (P*22/(RPM*3))

/*Public functions --------------------------------------------*/
void regulator_init(uint16_t P_lim, uint16_t Kp, uint16_t Ti, uint16_t Td)
{
    /* set the Power limit variable */
    p_limit = ((float)P_lim)/MAX_VALUE_16BIT;
    
    NVIC_InitTypeDef NVIC_initStruct;
    NVIC_initStruct.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_initStruct.NVIC_IRQChannelSubPriority = 3;
    NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_initStruct);
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseInitTypeDef TIM_timeBaseStruct;
    TIM_timeBaseStruct.TIM_Prescaler = 84-1; /* 42Mhz down to 1 Mhz */
    TIM_timeBaseStruct.TIM_Period = 10000 - 1; /* 1 Mhz down to 1Khz */ 
    TIM_timeBaseStruct.TIM_ClockDivision = 0;
    TIM_timeBaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_timeBaseStruct);
}
/**
* @brief : Disables the regulator loop interrupt.
*/
void regulator_disable(void)
{
    /* Disable interrupt */
    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
    /* Disable counter */
    TIM_Cmd(TIM2, DISABLE); 
    /*Request zero throttle from the VSI*/
    requestThrottle(0);
    /*Set the driver throttle value to be zero */
}
/**
* @brief : Enables the regulator loop interrupt.
*/
void regulator_enable(void)
{
    /* Enable interrupt */
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    /* Enable counter */
    TIM_Cmd(TIM2, ENABLE); 
    /*Make sure zero throttle is requested */
    requestThrottle(0);
}

/* Getters/setters ----------------------------------------------------------*/

void regulator_setVoltage(uint16_t dcLinkVoltage)
{    
		/*voltage = (float)(dcLinkVoltage/65535);*/
}

void regulator_setRPM(int16_t motorRPM)
{    
    RPM = (float)(motorRPM/32767);
}

void regulator_setCurrent(uint16_t batteryCurrent)
{    
		/*current = (float)(batteryCurrent/4095);*/
}

void regulator_setPlimit(uint16_t power_limit)
{

}

/**
* @brief : converts the 12 bit throttle value to a percentage value.
*/
void regulator_setThrottle(uint16_t throttleRequest)
{    
    if(throttleRequest > 4095)
    {
        throttleRequest = 4095;
    }
    throttle = (throttleRequest) << 3;
}

void regulator_setKp(uint16_t gain_p)
{
	
}

void regulator_setKi(uint16_t gain_i)
{
	
}
void regulator_setKd(uint16_t gain_d)
{
	
}

/* Private functions  ----------------------------------------------------------*/

/* Uses local variables for the gain constants. */
inline static float PID(float error, float iq_max)
{
//    float output = 0;
    /*TODO: PID */
    /*TODO: Anti windup */
    return 0;
}

    
inline static void requestThrottle(int16_t request)
{
    CanTxMsg iqMsg; 
    iqMsg.StdId = ID_VSI_TX;
    iqMsg.DLC   = 3;
    iqMsg.RTR   = 0;
    iqMsg.IDE   = 0;
    iqMsg.Data[0] = REG_REQUEST_TORQUE;
    iqMsg.Data[1] = MASK_LSB(request);
    iqMsg.Data[2] = MASK_MSB(request);
    
    while(CAN_Transmit(CAN2, &iqMsg) == CAN_TxStatus_NoMailBox);
}
/* Interrupt handlers -------------------------------------------------------*/
float error = 0; /* Regulator error */
float Iq = 0;    /* Current to be requested from the VSI */
float power = 0; /* Instantaneous power delivered from battery */

/* 
* The irq handler for the regulator. 
* Forces the regulator loop to execute
* at a constant rate.
*/
void TIM2_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        int16_t tempValue = (int16_t)throttle;
        if(tempValue < 0)
        {
            tempValue = 0;
        }
        tempValue = tempValue*(-1);
        requestThrottle(tempValue);
    }
}
