#include "io.h"
#include "stdlib.h"

void io_init(void)
{
    BSP_GPIOinitDigOut(GPIO_RTDS, PIN_RTDS);
    BSP_GPIOinitDigOut(GPIO_PILOTLINE, PIN_PILOTLINE);
    BSP_GPIOinitDigOut(GPIO_VSI_RUN, PIN_VSI_RUN);
    BSP_GPIOinitDigOut(GPIO_BRAKELIGHT, PIN_BRAKELIGHT);
    BSP_GPIOinitDigOut(GPIO_FAN_RADIATOR, PIN_FAN_RADIATOR);
    BSP_GPIOinitDigOut(GPIO_FAN_EXHAUST, PIN_FAN_EXHAUST);
    BSP_GPIOinitDigOut(GPIO_CAN1_EN, PIN_CAN1_EN);
    BSP_GPIOinitDigOut(GPIO_CAN2_EN, PIN_CAN2_EN);
    BSP_GPIOinitDigOut(GPIO_LED1, PIN_LED1);
    BSP_GPIOinitDigOut(GPIO_LED2, PIN_LED2);
    BSP_GPIOinitDigOut(GPIO_LED3, PIN_LED3);
    BSP_GPIOinitDigOut(GPIO_LED4, PIN_LED4);
}

void io_Toggle( gpioFunc_t io, gpioState_t state)
{
    switch (io)
    {
        case(exhaustFan):
        {
            BSP_GPIO_setOutput(GPIO_FAN_EXHAUST, PIN_FAN_EXHAUST, state);
            break;
        }
        case(radiatorFan):
        {
            BSP_GPIO_setOutput(GPIO_FAN_RADIATOR, PIN_FAN_RADIATOR, state);
            break;
        }
        case(can1):
        {
            BSP_GPIO_setOutput(GPIO_CAN1_EN, PIN_CAN1_EN, state);
            break;
        }
        case(can2):
        {
            BSP_GPIO_setOutput(GPIO_CAN2_EN, PIN_CAN2_EN, state);
            break;
        }
        case(inverterRunSignal):
        {
            BSP_GPIO_setOutput(GPIO_VSI_RUN, PIN_VSI_RUN, state);
            break;
        }
        case(brakelight):
        {
            BSP_GPIO_setOutput(GPIO_BRAKELIGHT, PIN_BRAKELIGHT, state);
            break;
        }
        case(pilotline):
        {
            BSP_GPIO_setOutput(GPIO_PILOTLINE, PIN_PILOTLINE, state);
            break;
        }
        case(RTDS):
        {
            BSP_GPIO_setOutput(GPIO_RTDS, PIN_RTDS, state);
            break;  
        }
        case(LED1):
        {
            BSP_GPIO_setOutput(GPIO_LED1, PIN_LED1, state);
            break;
        }
        case(LED2):
        {
            BSP_GPIO_setOutput(GPIO_LED2, PIN_LED2, state);
            break;
        }
        case(LED3):
        {
            BSP_GPIO_setOutput(GPIO_LED3, PIN_LED3, state);
            break;
        }
        case(LED4):
        {
            BSP_GPIO_setOutput(GPIO_LED4, PIN_LED4, state);
            break;
        }
    }
}

