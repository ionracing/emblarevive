#ifndef IOMAPPING_DISCOVERY_H
#define IOMAPPING_DISCOVERY_H

/* Constants for GPIOs that are used on the ECU */
#define PIN_RTDS PIN_LED1
#define GPIO_RTDS GPIO_LED1

#define PIN_PILOTLINE GPIO_Pin_0
#define GPIO_PILOTLINE GPIOC

#define PIN_VSI_RUN GPIO_Pin_2
#define GPIO_VSI_RUN GPIOC

#define PIN_BRAKELIGHT PIN_LED4
#define GPIO_BRAKELIGHT GPIO_LED4

#define PIN_FAN_RADIATOR GPIO_Pin_12
#define GPIO_FAN_RADIATOR GPIOB

#define PIN_FAN_EXHAUST GPIO_Pin_13
#define GPIO_FAN_EXHAUST  GPIOB

#define PIN_CAN1_EN GPIO_Pin_7
#define GPIO_CAN1_EN GPIOC

#define PIN_CAN2_EN GPIO_Pin_9
#define GPIO_CAN2_EN GPIOC

#define PIN_LED1 GPIO_Pin_12
#define PIN_LED2 GPIO_Pin_13
#define PIN_LED3 GPIO_Pin_14
#define PIN_LED4 GPIO_Pin_15
#define GPIO_LED1 GPIOD
#define GPIO_LED2 GPIOD
#define GPIO_LED3 GPIOD
#define GPIO_LED4 GPIOD

/* CAN-bus mapping ----------------------------------------------*/
/*CAN1*/
#define GPIO_CAN1_TX GPIOB
#define GPIO_CAN1_RX GPIOB
#define PIN_CAN1_TX GPIO_Pin_9
#define PIN_CAN1_RX GPIO_Pin_8
/*CAN2*/
#define GPIO_CAN2_TX GPIOB
#define GPIO_CAN2_RX GPIOB
#define PIN_CAN2_TX GPIO_Pin_6
#define PIN_CAN2_RX GPIO_Pin_5

#endif
