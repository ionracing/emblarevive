/* Defines ------------------------------------------------------------------------*/
#define FLASH_PREAMPLE 0xEF21
#define FLASH_START_ADRESS  0x080E0000
/* Macros ------------------------------------------------------------------------ */
#define FLASH_SENSOR_DATA_EXISTS *(uint16_t *)(FLASH_START_ADRESS) == FLASH_PREAMPLE
/* Global variables ---------------------------------------------------------------*/

/* Function prototypes ------------------------------------------------------------*/
void BSP_FLASH_storeCalibration(uint16_t data_high[8], uint16_t data_low[8]);
void BSP_FLASH_getCalibratedValues(uint16_t *data_high, uint16_t *data_low);
