#include "BSP_CAN.h"
/* 
* Could be written simpler since not all GPIO port/pin combinations are allowed for CAN.
*/

/* Private variables -------------------------------------*/
typedef struct
{
    CAN_TypeDef* CANx;
    uint16_t pinTx;
    uint16_t pinRx;
    uint16_t pinEnable;
    GPIO_TypeDef* GPIO_Tx;
    GPIO_TypeDef* GPIO_Rx;
    GPIO_TypeDef* GPIO_enable;
}canInit_t;

/* Private prototypes ---------------------------------------*/
void BSP_CAN_init(canInit_t* sCAN);

void BSP_CAN1_init(void)
{
    canInit_t can;
    can.CANx = CAN1;
    can.GPIO_enable = GPIO_CAN1_EN;
    can.GPIO_Rx = GPIO_CAN1_RX;
    can.GPIO_Tx = GPIO_CAN1_TX;
    can.pinEnable = PIN_CAN1_EN;
    can.pinRx = PIN_CAN1_RX;
    can.pinTx = PIN_CAN1_RX;
    BSP_CAN_init(&can);
}

void BSP_CAN2_init(void)
{
    canInit_t can;
    can.CANx = CAN2;
    can.GPIO_enable = GPIO_CAN2_EN;
    can.GPIO_Rx = GPIO_CAN2_RX;
    can.GPIO_Tx = GPIO_CAN2_TX;
    can.pinEnable = PIN_CAN2_EN;
    can.pinRx = PIN_CAN2_RX;
    can.pinTx = PIN_CAN2_RX;
    BSP_CAN_init(&can);
}

void BSP_CAN_init(canInit_t* sCAN)
{
    CAN_InitTypeDef CAN_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    CAN_FilterInitTypeDef CAN_FilterInitStructure;
    
    uint8_t fifoFilterN = 0; /* The FIFO filter which can uses = 0 or 1 */
    IRQn_Type rxIrq; /* The IRQ for CAN1 or CAN2*/
	/* Enable the CAN-bus tranciever by setting the enable pin high */
    BSP_GPIOinitDigOut(sCAN->GPIO_enable, sCAN->pinEnable);
    BSP_GPIO_setOutput(sCAN->GPIO_enable, sCAN->pinEnable, io_on);
    
    /* init pins as alternate function pins */
    BSP_GPIO_initAF(sCAN->GPIO_Rx, sCAN->pinRx);
	BSP_GPIO_initAF(sCAN->GPIO_Tx, sCAN->pinTx);
    
    /* Configure pins to alternate function and enable the CAN periph clock */
    if(sCAN->CANx == CAN1)
    {
        rxIrq = CAN1_RX0_IRQn;
        fifoFilterN = 0;
        GPIO_PinAFConfig(sCAN->GPIO_Rx, BSP_GPIO_pin2pinSource(sCAN->pinRx),GPIO_AF_CAN1 );
        GPIO_PinAFConfig(sCAN->GPIO_Tx, BSP_GPIO_pin2pinSource(sCAN->pinTx), GPIO_AF_CAN1);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE); 
    }
    else /* CAN2 is used */
    {
        rxIrq = CAN2_RX0_IRQn;
        fifoFilterN = 1;
        GPIO_PinAFConfig(sCAN->GPIO_Rx, BSP_GPIO_pin2pinSource(sCAN->pinRx),GPIO_AF_CAN2 );
        GPIO_PinAFConfig(sCAN->GPIO_Tx, BSP_GPIO_pin2pinSource(sCAN->pinTx), GPIO_AF_CAN2);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
    }

	/* CAN1 register init */
	CAN_DeInit(sCAN->CANx);

	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW  = CAN_SJW_3tq;                    

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/   
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    
	CAN_InitStructure.CAN_Prescaler = ((42000000 / 7) / 500000);   
	CAN_Init(sCAN->CANx, &CAN_InitStructure);

	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = fifoFilterN;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(sCAN->CANx, CAN_IT_FMP0, ENABLE);
    
	NVIC_InitStructure.NVIC_IRQChannel = rxIrq;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}	
