#ifndef INVERTER_H
#define INVERTER_H

/* Dependencies -------------------------------------------- */
#include "BSP_CAN.h"
#include "canMapping.h"
#include "delay.h"
/* MACROS ---------------------------------------------------*/
/*Constants -------------------------------------------------*/
/*Public functions ------------------------------------------*/

void inverter_init(uint8_t updateRate[10]);
#endif
