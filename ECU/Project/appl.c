/*Function prototypes*/
#include "regulator.h"
#include "storage.h"
#include "buffer.h"
#include "io.h"
#include "BSP_GPIO.h"
#include "BSP_CAN.h"
#include "inverter.h"
#include "delay.h"

/* Constants -------------------------------------------------------------*/
#define BUFFER_SIZE 32 /* Size of buffer must be a power of 2 */
const uint8_t standby = 1;
const uint8_t drive = 2;
const uint8_t passive = 3;
const uint8_t criticalError = 4;

#define IS_CAR_DRIVING (state == drive)
#define IS_CAR_STANDBY (state == standby)
#define IS_CAR_PASSIVE (state == passive)
#define IS_CAR_CRITICAL_ERROR (state == criticalerror)

/* Local variables --------------------------------------------------------*/
CanRxMsg can1rxmsg;
CanRxMsg can2rxmsg;
/* Circular buffer variables for CAN1 and CAN2 */
circBuffer_t bufferCan2_s; /* Circular buffer information struct */ 
circBuffer_t bufferCan1_s;
tElement bufferCan1[BUFFER_SIZE]; /* Buffer for can1 */
tElement bufferCan2[BUFFER_SIZE]; /* Buffer for can2 */

/* Flags -----------------------------------------------------------------*/
uint8_t precharged  = 0; /* Has the car been precharged? y/n (1/0)*/
uint8_t breaking    = 0; /* Is the driver breaking? y/n (1/0) */
uint8_t bwt         = 1; /* Is the driver throttling and breaking ? y/n (1/0)*/
#define READY_TO_START (breaking == 1)
uint8_t state = standby; /* Used for setting the car in different modes */

/* Local functins --------------------------------------------------------*/
static void driveMode(void);
static void standbyMode(void);
static void passiveMode(void);
static void criticalErrorMode(void);
static void VSImessageHandler(void);
static void updateParameter(CanRxMsg* msg);
static void setBrakeState(uint8_t state);
static void transmitError(uint8_t errorType, uint8_t state);
storageValues_t parameters; 
    
/* Main ------------------------------------------------------------------*/
/*
* Background task. Initializes modules, handles messages from CAN1 and CAN2.
* Regulator (Power limiter) runs in the regulator IRQ (see regulator.c).
* TODO : Handle error messages from VSI and BMS. (?)
* TODO : Check that temperatures are within range (IGBT, motor, VSI-air, BMS)
*/

int main(void)
{   
    /* Start Initialization --------------------------------------------- */
    systickInit(1000);
    /* Set standard values for non volatile parameters */
    uint8_t updateRates[10] = {50,50,50,50,200,0,200,200,200,200}; 
    for(uint8_t k= 0; k<10; k++)
    {
        parameters.loggUpdateRate[k] = updateRates[k]; 
    }
    /*TODO: Find a suitable parameter for regulator parameters */
    parameters.Kp = 2000; 
    parameters.Ki = 2000;
    parameters.Kd = 0;
    parameters.Plim = 2000;
    
    storage_saveValues(parameters);
    /* Retrieve any stored values */
    parameters = storage_readValues(parameters); 
    
    /* Init the buffers */
    buffer_init(&bufferCan2_s, bufferCan2,BUFFER_SIZE);
    buffer_init(&bufferCan1_s, bufferCan1,BUFFER_SIZE);
    
    io_init();
    PILOTLINE_ON; /* Precharging starts here */
    BSP_CAN_init();
    BSP_CAN2_init();
    delay_ms(1000); /* Let other modules on the bus startup before sending anything. */
    inverter_init(parameters.loggUpdateRate);
    regulator_init(parameters.Plim, parameters.Kp, parameters.Ki, parameters.Kd);
    /* Initialization done */
    while(1)
    {
        if(IS_CAR_DRIVING){ driveMode(); }
        else if(IS_CAR_STANDBY){ standbyMode(); }
        else if(IS_CAR_PASSIVE){ passiveMode(); }
        else {criticalErrorMode();}
    }
}

/**
* @brief : Background task while the motor is disengaged/disabled.
* Reads and processes canbus recieve packages.
*/
static void standbyMode(void)
{
    DISABLE_MOTOR;
    regulator_disable();
    breaking = 0; /* Needs to be reset to hinder immediate start */
    while(IS_CAR_STANDBY)
    {
        if(buffer_read(&bufferCan1_s, &can1rxmsg) != eErrorBufferEmpty)
        {
            switch(can1rxmsg.StdId)
            {
                /* Update non volatile parameters */
                case ID_PARAM_UPDATE: {updateParameter(&can1rxmsg);break;}
                /*Check if the driver is breaking */
                case ID_BRAKE :{setBrakeState(can1rxmsg.Data[0]); break;}
                /* Check if precharging is done */
                case ID_PRECHARGE : {precharged = (can1rxmsg.Data[0] == 1 ? 1:0); break;} 
                /* Check if the driver is throttling while breaking */
                case ID_BWT : {bwt = (can1rxmsg.Data[0]) == 1 ? 1 : 0; break;}
                case ID_START :
                {
                    if(READY_TO_START)
                    {
                        state = drive;
                        RTDS_ON;
                        delay_ms(1000);
                        RTDS_OFF;
                    }
                    break;
                }
            }
        }
        VSImessageHandler();
    }
}
/**
* @brief : Background task while the motor is engaged/enabled.
* Reads and processes canbus recieve packages.
*/

static void driveMode(void)
{
    regulator_enable();
    /* Turn on fans and pumps */
    RADIATOR_FAN_ON;
    EXHAUST_FAN_ON;
    ENABLE_MOTOR;
    while(IS_CAR_DRIVING)
    {
        if(buffer_read(&bufferCan1_s, &can1rxmsg) != eErrorBufferEmpty)
        {
            switch(can1rxmsg.StdId)
            {
                /* If breaking while throttling enter passive mode*/
                case ID_BWT:{state = (can1rxmsg.Data[0] == 1 ? passive : drive);break;}  
                /* Toggle on/off brakelight */
                case ID_BRAKE :{setBrakeState(can1rxmsg.Data[0]); break;}
                /*TODO: Make implausability result in passive mode */
                case ID_PEDALS_IMP : { state = criticalError; break; }
                case ID_THROTTLE : /* Throttle request from driver */
                {
                    regulator_setThrottle(BYTE2HALFWORD(can1rxmsg.Data[1],can1rxmsg.Data[0]));
                    break;
                }
                case ID_BMS_VOLTAGE :
                {
                    /*BMS transmits in BIG-ENDIAN */
                    regulator_setVoltage(BYTE2HALFWORD(can1rxmsg.Data[0],can1rxmsg.Data[1]));
                    break;
                }
                case ID_BMS_CURRENT :
                {
                    /*Sensor modules transmits in little-endian*/
                    regulator_setCurrent(BYTE2HALFWORD(can1rxmsg.Data[1],can1rxmsg.Data[0]));
                    break;
                }
                case ID_START : /*Driver pushed the stop button */
                {
                    state = standby;
                    break;
                }
            }
        }
        /* Logg data from VSI */
        VSImessageHandler();
    }
}
/**
* @brief : Car enters passive mode if a non-vital error occours,
* for example if the driver is throttling while driving.
* while the error lasts the motor is disabled.
*/
static void passiveMode(void)
{
  //  DISABLE_MOTOR;
  //  regulator_disable();
  //  delay_ms(100);
    transmitError(ERROR_TYPE_BWT, ERROR_TRUE);
//    while(IS_CAR_PASSIVE)
//    {
//        if(buffer_read(&bufferCan1_s, &can1rxmsg) != eErrorBufferEmpty)
//        {
//            if(can1rxmsg.StdId == ID_BWT && (can1rxmsg.Data[0] == 0))
//            {
//                state = drive;
//            }
//        }
//        /* Stil want to logg data from the VSI */
//        VSImessageHandler();
//    }
    transmitError(ERROR_TYPE_BWT, ERROR_FALSE);
//    ENABLE_MOTOR;
    state = drive;
    /* Regulator is enabled in the start of drive loop*/
}


/**
* @brief : Car enters this state if a severe error occours.
* Microcontroller enters a infinite loop. Only way to start driving again
* is to reset the 12v system.
*/
static void criticalErrorMode(void)
{
    //regulator_disable();
    //PILOTLINE_OFF;
    //DISABLE_MOTOR;
    transmitError(ERROR_TYPE_CRITICAL, ERROR_TRUE);
    state = drive;
}


/**
* @brief : Transmits an error message on CAN1.
* @input errorType : value for the error (ERROR_TYPE_x)
* @input state : error state can be either ERROR_TRUE or ERROR_FALSE
* @note : The input values are defined in canMapping.h.
*/
static void transmitError(uint8_t errorType, uint8_t state)
{
    CanTxMsg errorMsg;
    errorMsg.StdId   = ERROR_ID;
    errorMsg.DLC     = 2;
    errorMsg.IDE     = 0;
    errorMsg.Data[0] = errorType;
    errorMsg.Data[1] = state;
    while(CAN_Transmit(CAN1, &errorMsg) == CAN_TxStatus_NoMailBox);
    while(CAN_Transmit(CAN1, &errorMsg) != CAN_TxStatus_NoMailBox);
}

/**
* @brief Handles messages from the VSI. 
* Transmits all packages on CAN1 for logging.
* Updates the regulator if it is a RPM package.
* TODO: Check if temperatures are within range!
*/
static void VSImessageHandler(void)
{
    if(buffer_read(&bufferCan2_s, &can2rxmsg) != eErrorBufferEmpty)
    {
        if(can2rxmsg.StdId == ID_VSI_RX) 
        {
            if(can2rxmsg.Data[1] == REG_SPEED)
            {
                regulator_setRPM(BYTE2HALFWORD(can2rxmsg.Data[1],can2rxmsg.Data[0]));
            }
            CanTxMsg can2txmsg;
            /* Copy the receive package in to a transmit package */
            for(uint8_t k = 0; k<8; k++)
            {
                can2txmsg.Data[k] = can2rxmsg.Data[k];
            }
            can2txmsg.StdId = can2rxmsg.StdId;
            can2txmsg.IDE = 0;
            can2txmsg.RTR   = can2rxmsg.RTR;
            can2txmsg.DLC   = can2rxmsg.DLC;
            /* Transmit package for logging */
            CAN_Transmit(CAN1, &can2txmsg);
        }
    } 
}

/**
* @brief: Loops through parameter update data.
* updates the private parameter list. 
* updates the module which uses the parameter.
* stores the parameter.
*/
static void updateParameter(CanRxMsg* msg)
{
    if(msg->Data[0] <= regLoggUpdateRate)
    {
        uint16_t var = BYTE2HALFWORD(msg->Data[2], msg->Data[1]);
        switch(msg->Data[0])
        {
        case regKp :
            {
                parameters.Kp = var;
                regulator_setKp(var);
                break;
            }
        case regKi :
            {
                parameters.Ki = var;
                regulator_setKi(var);
                break;
            }
        case regKd :
            {
                parameters.Kd = var;
                regulator_setKd(var);
                break;
            }
        case regPlim :
            {
                parameters.Plim = var;
                regulator_setPlimit(var);
                break;
            }
        case regLoggUpdateRate :
            {
                if(msg->Data[1] > 0 && msg->Data[1] <10 && msg->DLC == 3) /* if message is within range */
                {
                    parameters.loggUpdateRate[msg->Data[1]] = msg->Data[2];
                    /* Updates the logging rate of VSI messages */
                    inverter_init(parameters.loggUpdateRate); 
                }
                break;
            }
        }
       storage_saveValues(parameters); /* Store the new value in non-volatile memory */
    } 
}

static void setBrakeState(uint8_t state)
{
    breaking = state;
    if(state == 1){BRAKELIGHT_ON;}
    else{BRAKELIGHT_OFF;}    
}

/* Interrupt handlers -----------------------------------------------------------*/

/*
Interrupt handlers for CAN1 and CAN2. Simply reads messages from
the FIFO recieve buffer and puts them in a larger circular buffer.
The circular buffer gets looped through in the background task. 
The interrupts are initialized in BSP_CAN.h 
*/

/**
* @brief : IRQ-handler for message recieved on CAN1
* @note : CAN1 is configured to recieve messages on FIFO0
*/
void CAN1_RX0_IRQHandler(void)
{
	CanRxMsg rxmsg;
    CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);
    buffer_write(&bufferCan1_s, rxmsg);
}

/**
* @brief : IRQ-handler for message recieved on CAN2
* @note : CAN2 is configured to recieve messages on FIFO1
*/
void CAN2_RX0_IRQHandler(void)
{
    CanRxMsg rxmsg;
    CAN_Receive(CAN2, CAN_FIFO0, &rxmsg);
    buffer_write(&bufferCan2_s, rxmsg);
}
