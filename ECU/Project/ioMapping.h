#ifndef IOMAPPING_H
#define IOMAPPING_H

#if defined COMPILING_FOR_DEVBOARD
    #warning "compiling for development board!"
    #include "ioMapping_discovery.h"
#elif defined COMPILING_FOR_V1
    #include "ioMappingV1.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
