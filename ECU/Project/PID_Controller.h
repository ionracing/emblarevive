#include "appl.h"

typedef struct 
{
	float integrator_max; 
	float integrator_min;
	float prev_error;
	float integral_error;
	float pGain;
	float iGain;
	float dGain;

}PID;

float updatePID(PID * pid, float current_error);


