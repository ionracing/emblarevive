/* Includes */
#include "stm32f4xx.h"
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "BSP_FLASH.h"

#define FLASH_ADRESS(k) FLASH_START_ADRESS + (1 + k)*16

uint16_t storageArray[50] = {0};
/* Stores a array of 16 bit values to the memory */
void BSP_FLASH_storeCalibration(uint16_t data[50])
{
	uint8_t i;
	FLASH_Unlock();
	FLASH_ClearFlag( FLASH_FLAG_EOP |  FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	FLASH_EraseSector(FLASH_Sector_11, VoltageRange_3); 
	
	/* Used for determening if the calibration has been saved previously */
	FLASH_ProgramHalfWord(FLASH_START_ADRESS, FLASH_PREAMPLE); 
	
	/* Save the data*/
	for(i=0; i<=7; i++)
	{
		FLASH_ProgramHalfWord(FLASH_ADRESS(i), data[i]);
	}
	/*The flash must be locked again when the writing is finished */
	FLASH_Lock();
}


/**
 * @brief read the stored values.
*/
uint8_t BSP_FLASH_getCalibratedValues(uint16_t *data)
{
    if(FLASH_SENSOR_DATA_EXISTS)
	{
        int i = 0;
		for(i=0; i<=7; i++)
        {
            data[i]  = *(uint16_t*)(FLASH_ADRESS(i));		
        }
        return 0;
	}
	else  /* Not data is stored */
	{
        return 1;
	}
}