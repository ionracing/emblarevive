#ifndef REGULATOR_H
#define REGULATOR_H

/* Dependencies -------------------------------------------- */
    #include "stdint.h"
    #include "stm32f4xx_tim.h"
    #include "canMapping.h"
    #include "BSP_CAN.h"
    #include "delay.h"

/* MACROS ---------------------------------------------------*/
/*Constants -------------------------------------------------*/
#define VOLTAGE_MAX 65535   /* 0 to 500V*/
#define RPM_MAX 32767       /* +\- 4500 RPM */
#define CURRENT_MAX 4095    /* 0 to 250A */
#define THROTTLE_MAX 4095   /* 0 to 100% */

/*Public functions ------------------------------------------*/

/**
* @brief : Initializes the regulator and the regulator variables.
*          Uses a IRQ-handler for the controll loop.
* @note  : The functions regulator_enable() and regulator_disable() 
*   is used for turning on/off the interrupt. Regulator_enable() must be
*   called after initializeing the regulator for the interrupts to start.
* @input P_lim : The maximum power allowed from the motor.
* @input Kp    : The gain for the control loop.
* @input Ki    : The integral gain for the control loop.
* @input Kd    : The derivative gain for the control loop.
*/
void regulator_init(uint16_t P_lim, uint16_t Kp, uint16_t Ti, uint16_t Td);
/* Disables the regulator IRQ */
void regulator_enable(void);
/* Enables the regulator IRQ */
void regulator_disable(void); 
/* set functions: 
* Used for setting the variables in the regulator loop.
*/

/**
* @brief : Updates the battery voltage to the regulator.
* @input : Voltage 0 to 65535 ( 0 to 500V )
*/
void regulator_setVoltage(uint16_t voltage);

/**
* @brief : Updates the current motor RPM to the regulator loop.
* @input motorRPM : A value between +/- 4500 RPM = +/-32767
*/
void regulator_setRPM(int16_t motorRPM);

/**
* @brief : Updates the output current from the battery to 
*   the regulator loop.
* @input batteryCurrent : value between 0 to 4095 (0 to 250A)
*/
void regulator_setCurrent(uint16_t batteryCurrent);

/**
* @brief : Updates the driver torque request to the regulator loop.
* @input throttleRequest : value between 0 to 4095 (0 to 100% torque request) 
*/
void regulator_setThrottle(uint16_t throttleRequest);

/**
* @brief : Updates the maximum power to the regulator loop.
* @input power : unsigned value between 0 to 65536 (0 to 125kW) 
*/
void regulator_setPlimit(uint16_t power);

void regulator_setKd(uint16_t gain_d);
void regulator_setKi(uint16_t gain_i);
void regulator_setKp(uint16_t gain_p);
#endif
