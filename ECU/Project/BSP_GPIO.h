#ifndef BSP_GPIO_H
#define BSP_GPIO_H


/* Dependencies -------------------------------------------- */
#include "stm32f4xx_gpio.h"
#include "ioMapping.h"
/* MACROS ---------------------------------------------------*/
#define GPIO_HIGH(ch,pin) (ch->BSRRL |= pin)
#define GPIO_LOW(ch,pin) (ch->BSRRH |= pin)
#define DISABLE_MOTOR (GPIO_LOW(GPIO_VSI_RUN, PIN_VSI_RUN))
#define ENABLE_MOTOR (GPIO_HIGH(GPIO_VSI_RUN, PIN_VSI_RUN))
#define BRAKELIGHT_ON (GPIO_HIGH(GPIO_BRAKELIGHT, PIN_BRAKELIGHT))
#define BRAKELIGHT_OFF (GPIO_LOW(GPIO_BRAKELIGHT, PIN_BRAKELIGHT))
#define EXHAUST_FAN_ON (GPIO_HIGH(GPIO_FAN_EXHAUST, PIN_FAN_EXHAUST))
#define EXHAUST_FAN_OFF (GPIO_LOW(GPIO_FAN_EXHAUST, PIN_FAN_EXHAUST))
#define RADIATOR_FAN_ON (GPIO_HIGH(GPIO_FAN_RADIATOR, PIN_FAN_RADIATOR))
#define RADIATOR_FAN_OFF (GPIO_LOW(GPIO_FAN_RADIATOR, PIN_FAN_RADIATOR))
#define RTDS_ON (GPIO_HIGH(GPIO_RTDS, PIN_RTDS))
#define RTDS_OFF (GPIO_LOW(GPIO_RTDS, PIN_RTDS))
#define PILOTLINE_ON (GPIO_HIGH(GPIO_PILOTLINE, PIN_PILOTLINE))
#define PILOTLINE_OFF (GPIO_LOW(GPIO_PILOTLINE, PIN_PILOTLINE))
/*Constants -------------------------------------------------*/
/* Used for setting the GPIO high or low */
typedef enum
{
    io_on,
    io_off,
}gpioState_t;

/*Public functions ------------------------------------------*/
void BSP_GPIOinitDigIn(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void BSP_gpioEnableClock(GPIO_TypeDef* GPIOx);
void BSP_GPIOinitDigOut(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void BSP_GPIO_setOutput(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, gpioState_t state);

/** @brief : Initializes the GPIO as a alternate function.
* @Note: for using the pins you have to configure them as 
* as a alternate function using the function GPIO_PinAFConfig().
* @input GPIOx : The GPIO port to be initialized.
* @input GPIO_Pin : The GPIO Pin to be initialized.
* @retval : None
*/
void BSP_GPIO_initAF(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint8_t BSP_GPIO_pin2pinSource(uint16_t gpioPin);
#endif

