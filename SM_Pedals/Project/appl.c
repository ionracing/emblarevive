/* Includes */
#include "stdbool.h"
#include "stm32f4xx.h"
#include "BSP_LED.h"
#include "BSP_systick.h"
#include "stm32f4xx_tim.h"
#include "BSP_CAN.h"
#include "BSP_ADC_SIMPLE.h"
#include "core_cm4.h"
#include "ioMapping.h"
#include "BSP_ADC_SIMPLE.h"
#include "BSP_GPIO.h"

/*Function prototypes*/
static void heartbeat(void);
static void initSystem(void);

/* Main code */
int main(void)
 {
     /* Init all the system functions (CAN, GPIO, ADC) */
     initSystem();
     
     /* Read ADC channels */
     /* Check if readings are within range */
     /* Transmit data on CAN */
     
 } 

/* Misc functions *****************************************************/
static void heartbeat(void)
{
    if (clk100ms == CLK_COMPLETE)
    {
        LED_TOGGLE(LED1);
        clk100ms = CLK_RESET;
	}
}

static void initSystem(void)
{
	BSP_systick_init();
	BSP_LED_init();
	BSP_ADC_SIMPLE_init();
	BSP_CAN_init();
    BSP_GPIOinit();
}
